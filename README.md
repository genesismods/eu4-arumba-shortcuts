Arumba's Keyboard Shortcuts for Europa Universalis IV
---

## Installation

1) Subscribe on the [Steam Workshop](http://steamcommunity.com/sharedfiles/filedetails/?id=1205516856)

*-or-*

2) Download the [zipball from Gitlab](https://gitlab.com/Genesis2001/eu4-arumba-shortcuts/repository/master/archive.zip) and extract the contents of the folder into your `Documents\Paradox Interactive\Europa Universalis IV\mod\` folder.

The mod folder should look like this:

```
\ (mod folder)
\ArumbaShortcuts.mod
\ArumbaShortcuts\
```

## Disclaimers

This repository is not associated with Arumba at all. The workshop mods by me are uploaded with permission from his mod team.

## Links to Arumba

* https://www.youtube.com/user/arumba07/featured
* https://discord.gg/arumba
* https://www.reddit.com/r/arumba07
